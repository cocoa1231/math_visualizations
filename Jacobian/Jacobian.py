#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter

fig = plt.figure(figsize=(15, 15))
ax  = fig.gca(projection='3d')

x, y, z = np.meshgrid(np.arange(-10, 10, 0.5),
                      np.arange(-10, 10, 0.5),
                      np.arange(-0.1, 0.1, 1))

u = x*y
v = x
w = z**0 - 1

ax.quiver(x, y, z, u, v, w, normalize = True)

X, Y = np.meshgrid(np.linspace(-10, 10, 50),
                   np.linspace(-10, 10, 50))

Z = Y - X

surf = ax.plot_surface(X, Y, Z, antialiased=False)

# Customize the z axis.
ax.set_zlim(-10.01, 10.01)
# ax.zaxis.set_major_locator(LinearLocator(10))
# ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

ax.set_xlabel("$X$")
ax.set_ylabel("$Y$")
ax.set_zlabel("$Z$")

plt.show()


